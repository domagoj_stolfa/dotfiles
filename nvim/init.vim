set number			" Show line numbers
set linebreak			" Break lines at word (requires Wrap lines)
set showbreak=+++		" Wrap-broken line prefix
set textwidth=80		" Line wrap (number of cols)
set showmatch			" Highlight matching brace
set visualbell			" Use visual bell (no beeping)
 
set hlsearch			" Highlight all search results
set smartcase			" Enable smart-case search
set ignorecase			" Always case-insensitive
set incsearch			" Searches for strings incrementally
 
set autoindent			" Auto-indent new lines
set cindent			" Use 'C' style program indenting
set softtabstop=8		" Number of spaces per Tab
 
set ruler			" Show row and column ruler information
set rnu
set colorcolumn=80
 
set undolevels=1000		" Number of undo levels
set backspace=indent,eol,start	" Backspace behaviour

set guicursor=

" clang-format related variables. To use these, install clang-format.
let g:clang_format#enable_fallback_style=0
let g:clang_format#detect_style_file=1

" hjkl -> km,.
noremap . l
noremap m k
noremap , j
noremap k h

" ctags
set tags=./tags,tags;$HOME

" fzf
nnoremap <silent> <Leader>b :Buffers<CR>
nnoremap <silent> <C-f> :Files<CR>
nnoremap <silent> <Leader>f :Rg<CR>
nnoremap <silent> <Leader>/ :BLines<CR>
nnoremap <silent> <Leader>' :Marks<CR>
nnoremap <silent> <Leader>g :Commits<CR>
nnoremap <silent> <Leader>H :Helptags<CR>
nnoremap <silent> <Leader>hh :History<CR>
nnoremap <silent> <Leader>h: :History:<CR>
nnoremap <silent> <Leader>h/ :History/<CR> 

command! -bang -nargs=* Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, {'options': '--delimiter : --nth 4..'}, <bang>0)

" ocamlformat things. Use janestreet by default.
let g:neoformat_ocaml_ocamlformat = {}
let g:neoformat_ocaml_ocamlformat.exe = 'ocamlformat'
let g:neoformat_ocaml_ocamlformat.args = ['--inplace', '--enable-outside-detected-project', '-p janestreet']
let g:neoformat_ocaml_ocamlformat.replace = 1
let g:neoformat_enabled_ocaml = ['ocamlformat']

" Make latexindent work inplace for LaTeX files.
let g:neoformat_tex_latexindent = {
  \ 'exe': 'latexindent',
  \ 'stdin': 1
  \ }

" For the most part, use NeoFormat to format things. clang-format is an
" exception.
autocmd FileType ocaml map <Leader>` :Neoformat<Cr>
autocmd FileType tex map <Leader>` :Neoformat<Cr>

" Special settings for ocaml
au BufEnter *.ml setf ocaml
au BufEnter *.mli setf ocaml
au FileType ocaml call FT_ocaml()
function FT_ocaml()
    set textwidth=120
    set colorcolumn=120
    set shiftwidth=2
    set tabstop=2
    filetype indent on
    filetype plugin indent on
endfunction

" clang-format binding
vmap <Leader>c :ClangFormat<CR>

call plug#begin('~/.config/nvim/plug')

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'rhysd/vim-clang-format'
Plug 'sbdchd/neoformat'
Plug 'kien/ctrlp.vim'
Plug 'conornewton/vim-latex-preview'
Plug 'jonathanfilip/vim-lucius'
Plug 'vimwiki/vimwiki'

call plug#end()

" Source the opam file (if exists)
runtime opam.vim

" Bind MerlinRename and MerlinRenameAppend
nmap <LocalLeader>r  <Plug>(MerlinRename)
nmap <LocalLeader>R  <Plug>(MerlinRenameAppend)

" LaTeX preview
let g:latex_pdf_viewer="mupdf"

nmap <LocalLeader>l :StartLatexPreview<CR>

" colorscheme, default to LuciusBlack
colorscheme lucius
LuciusBlack

