alias vim='nvim'
export QEMU_PA_SAMPLES=4096

alias odig='BROWSER=w3m odig'
export TERM=xterm-256color

export PS1="\[\033[38;5;11m\]\u\[$(tput sgr0)\]@\h:\[$(tput sgr0)\]\[\033[38;5;6m\][\w]\[$(tput sgr0)\]\\$ \[$(tput sgr0)\]"
export LS_COLORS="di=1;31"

export SSH_AUTH_SOCK=${HOME}/.ssh/agent
if ! pgrep -u ${USER} ssh-agent > /dev/null; then
    rm -f ${SSH_AUTH_SOCK}
fi
if [ ! -S ${SSH_AUTH_SOCK} ]; then
    eval $(ssh-agent -a ${SSH_AUTH_SOCK} 2> /dev/null)
fi


test -r /home/ds815/.opam/opam-init/init.sh && . /home/ds815/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true


PATH="/home/ds815/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/ds815/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/ds815/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/ds815/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/ds815/perl5"; export PERL_MM_OPT;

alias ls="ls --color=always"

