import os
import subprocess

def mailpasswd(acct):
  acct = os.path.basename(acct)
  home = os.path.expanduser('~')
  path = (home + "/passwords/%s-muttpass.gpg") % acct
  args = ["gpg", "--use-agent", "--quiet", "--batch", "-d", path]
  try:
    return subprocess.check_output(args).strip()
  except subprocess.CalledProcessError:
    return ""

